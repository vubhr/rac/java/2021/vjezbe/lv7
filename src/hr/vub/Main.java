package hr.vub;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        //Zadatak 1.
        Runnable r = () -> System.out.println("Test");
        r.run();

        //Zadatak 2.
        Ispis t = (s) -> System.out.println(s);
        t.ispis("Test");

        //Zadatak 3.
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        Consumer<Integer> c = (n) -> {System.out.println(n);};
        list.forEach(n-> System.out.println(n));
        list.forEach(c);

        //Zadatak 4.
        Predicate<Integer> p = n -> (n%2 ==0) ? true : false;
        List<Integer> out = list.stream().filter(p).collect(Collectors.toList());
        System.out.println("Parni brojevi:");
        out.forEach(c);

        //Zadatak 5 i 6.
        Random rand = new Random();
        float leftLimit = 1.0F;
        float rightLimit = 5.0F;

        List<Student> students = new ArrayList<Student>();
        for(int i = 0; i < 100; i++){
            float generatedFloat = leftLimit + new Random().nextFloat() * (rightLimit - leftLimit);
            students.add(new Student(generatedFloat,"Ime" + i, "Prezime" + i, rand.nextBoolean()));
        }
        List<Student> topStudents = students.stream()
                                    .filter(student -> student.getGPA() >= 4.5)
                                    .collect(Collectors.toList());
        System.out.println("Best students: ");
        topStudents.forEach(student -> System.out.println(student.getFirstname() + " " +
                        student.getLastname()  + "  " + student.getGPA()));

        //Zadatak 6.
        Predicate<Student> top = student -> student.getGPA() >= 4.5;
        Predicate<Student> topFinished = top.and(student -> student.getFinished());
        List<Student> topFinishedStudents = students.stream()
                .filter(topFinished)
                .collect(Collectors.toList());

        System.out.println("Best finished:");
        Consumer<Student> st = (student) -> System.out.println(
                student.getFirstname() + " " + student.getLastname() + " " +
                        student.getGPA() + " " +student.getFinished() );
        topFinishedStudents.forEach(st);
    }
}

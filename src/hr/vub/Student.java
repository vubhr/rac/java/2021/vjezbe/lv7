package hr.vub;

public class Student {
    private float  GPA= 0;
    private String firstname;
    private String lastname;
    private boolean finished;


    public Student(float GPA, String firstname, String lastname, boolean finished) {
        this.GPA = GPA;
        this.firstname = firstname;
        this.lastname = lastname;
        this.finished = finished;
    }

    public float getGPA() {
        return GPA;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
    public boolean getFinished(){
        return finished;
    }

}
